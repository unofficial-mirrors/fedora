Fedora Dockerfiles build wrapper
================================

!["Prompt"](https://raw.githubusercontent.com/gbraad/assets/gh-pages/icons/prompt-icon-64.png)


Build wrapper for [Fedora-Dockerfiles](https://gitlab.com/gbraad/Fedora-Dockerfiles)[*](https://github.com/gbraad/Fedora-Dockerfiles) images.


Usage
-----

### Cloned

  * Fedora base (24)  
    `docker pull registry.gitlab.com/gbraad/fedora:24`


### Custom

  * Fedora Ansible  
    `docker pull registry.gitlab.com/gbraad/fedora:ansible`
  * Fedora Atomic-reactor  
    `docker pull registry.gitlab.com/gbraad/fedora:atomicreactor`  
    for usage instructions: [Atomic reactor](https://github.com/gbraad/knowledge-base/blob/master/technology/atomic.md#atomic-reactor)
  * Fedora Systemd  
    `docker pull registry.gitlab.com/gbraad/fedora:systemd`  
    for usage instructions: [systemd](./systemd/README.md)


Authors
-------

| [!["Gerard Braad"](http://gravatar.com/avatar/e466994eea3c2a1672564e45aca844d0.png?s=60)](http://gbraad.nl "Gerard Braad <me@gbraad.nl>") |
|---|
| [@gbraad](https://twitter.com/gbraad)  |
